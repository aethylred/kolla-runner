# Kolla Runner Image

Docker image for running kolla container builds in GitLab CI (docker-in-docker).

# Runner Image Usage

The runner images created by GitLab CI with this repository are not intended to be used directly by Kolla, but only for building Kolla container images with a [Kolla Image Builder](https://gitlab.com/aethylred/kolla-image-builder).

## Build Runner Image

1. Push or mirror this repository into a GitLab instance with CI/CD runners set up. 
2. Edit, commit, and push any customisations or chaged (e.g. adding patches) to the desired release `Dockerfile` in the [`releases` directory](releases).
3. Manually trigger the build pipelines for the release being targetted.

## Use Runner Image

Clone [Kolla Image Builder](https://gitlab.com/aethylred/kolla-image-builder) and create a branch from `main` to customise. To use these runner images update the `builder_image` keys in the configuration with the URI for the release image to be worked with. See the example below:

```
ussuri:
    name: Ussuri Release
    builder_image: registry.gitlab.com/aethylred/kolla-runner/by_release:ussuri
```

# Distribution Image Usage

The intention of the distribution images is to build a local docker image that Kolla can use to build the Kolla component images, which should enable Kolla deployments without direct internet connections.

## Build Distribution Image

1. Push or mirror this repository into a GitLab instance with CI/CD runners set up. 
2. Edit, commit, and push any customisations or chaged (e.g. adding packages or repositories) to the Docker file in the [`distro` directory](distro)
3. Manually trigger the build pipelines for the distribution to be used

## Use Distribution Image

Clone [Kolla Image Builder](https://gitlab.com/aethylred/kolla-image-builder) and create a branch from `main` to customise.

TBD - Still figuring this out :)

# Development

## By Release
The intention with the `by_release` branch is to modify the runner to produce a container image pre-loaded with everything required to build the stable branch of each OpenStack release with Kolla. The intent here is to improve build stability and to reduce build time of each OpenStack component image with Kolla. With the pre `1.0.0` version each image had to build its Kolla config with `tox` and download release specific packages and modules.

# Features

## Clair Scanner
The runners include the [Clair Scanner](https://github.com/arminc/clair-scanner) allowing the image to also be used to run Clair vunerability scans.

# License

[Apache 2.0](LICENSE)

# References

This work is derived from [Kolla Runner](https://gitlab.ics.muni.cz/cloud/kolla-runner/) by [Andrei Kirushchanka](https://gitlab.ics.muni.cz/andy1609)