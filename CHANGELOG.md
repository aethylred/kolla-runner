# Version History

## Version 1.0.0
- Build runner images by release, starting with Ussuri and Victoria
- Build a base distribution to build Kolla images from, CentOS 8-Stream example provided
- Include `patches` directory with each release `Dockerfile` and examples on how to use them to patch Kolla.

## Version 0.1.0
- Alpine no long supports Python 2 directly (Python 2.7 is EoL)... disabled rather than removed
- pip3 and pip are not quite interchangable
- tox must now be installed as a package
- ~~Suppress disable detached head warning to reduce line count~~ Removed as this was informative.

## Version 0.0.3
- Mercilessly copied from [Kolla Runner](https://gitlab.ics.muni.cz/cloud/kolla-runner/)
